#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}



bool carPurchase(int buyerid, int carid, sqlite3*db, char*zErrMsg)
{
	int rc, carsPrice, balance;
	string str;

	str = "select available from cars where id=" + to_string(carid) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);

	if ((*results.begin()).second[0] == "0")
	{
		clearTable();
		return false;
	}
	clearTable();
	
	str = "select balance from accounts where Buyer_id=" + to_string(buyerid) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (results.size() > 0)
	{
		balance = atoi((*results.begin()).second[0].c_str());
	}
	str = "select price from cars where id=" + to_string(carid) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	
	if (sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg) != SQLITE_OK) //checking the price of the car
	{
		system("cls");
		system("pause");
		std::cout << zErrMsg << std::endl;
		return 0;
	}
	if (results.size() > 0)
	{
		carsPrice = atoi((*results.begin()).second[0].c_str());
	}
	if (balance < carsPrice)
	{
		return false;
	}
	
	str = "update cars set available = 0 where id=" + to_string(carid) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	balance = balance - carsPrice;
	str = "update accounts set balance =" + 
		  to_string(balance) + 
		  "where Buyer_id=" + 
		  to_string(buyerid) + ";";
	sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

	return true;
}

bool balanceTransfer(int from, int to, int amount, sqlite3*db, char* zErrMsg)
{
	int rc, balanceFrom, balanceAmount, balance;
	string str;
	str = "select balance from accounts where id=" + to_string(from) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (results.size() > 0)
	{
		balance = atoi((*results.begin()).second[0].c_str());
	}
	if (balance < amount)
	{
		return false;
	}
	balance = balance - amount;
	str = "update accounts set balance=" + to_string(balance) + "where id=" + to_string(from) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	str = "select balance from accounts where id=" + to_string(to) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	if (results.size() > 0)
	{
		balance = atoi((*results.begin()).second[0].c_str());
	}
	balance = balance + amount;
	str = "update accounts set balance=" + to_string(balance) + "where id=" + to_string(to) + ";";
	sqlite3_exec(db, str.c_str(), NULL, 0, &zErrMsg);
	return true;

}



int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	carPurchase(5, 18,db, 0);
	carPurchase(12, 22, db, 0);
	carPurchase(10, 16, db, 0);
	system("pause");
	return 0;
}